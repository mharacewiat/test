Test
===

# Description

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vestibulum libero tempor scelerisque lacinia. Aenean ac velit lobortis, mattis enim eu, maximus diam. Etiam lobortis nec metus in pharetra. In a mi mauris. Quisque sagittis semper nunc, vel fringilla augue ultricies non. Vivamus massa tortor, convallis convallis justo eget, posuere iaculis eros. Sed cursus consequat purus, ac pulvinar mi suscipit eget. Donec consequat nisi vel tortor commodo, ac imperdiet nunc ultricies. Ut sed efficitur massa.

# Installation

```bash
git clone git@github.com:Haru0/test.git
composer install
```

# Tests

```bash
vendor/bin/phpunit --coverage-html tests/coverage
```
