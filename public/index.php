<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Manana\Calculator\CalculatorFactory;
use Manana\Importer\MatchesImporter;
//use Manana\Resolver\WinningTeamResolver;

$importer = new MatchesImporter();
$matches = $importer->import(__DIR__ . '/../resources/matches');

foreach ($matches as $match) {
    try {
        $calculator = CalculatorFactory::factory($match->getType());
    } catch (\Exception $exception) {
        dump($exception->getMessage());
        continue;
    }

    try {
        $calculator->calculate($match);
    } catch (\Exception $exception) {
        dump($exception->getMessage());
        continue;
    }
}

//$winningTeamResolver = new WinningTeamResolver();
//$winningTeam = $winningTeamResolver->resolve($matches);

//$bestPlayer = $matches;

dump(
    $matches
//    ,
//    $bestPlayer
);
