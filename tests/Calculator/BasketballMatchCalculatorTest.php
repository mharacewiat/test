<?php

namespace Tests\Calculator;

use Manana\Calculator\BasketballMatchCalculator;
use Manana\Model\BasketballPlayer;
use Manana\Model\Match;
use PHPUnit\Framework\TestCase;

/**
 * BasketballMatchCalculatorTest class.
 *
 * @package Tests\Calculator
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class BasketballMatchCalculatorTest extends TestCase
{

    public function testCalculate()
    {
        $match = new Match(Match::TYPE_BASKETBALL, [
            new BasketballPlayer(BasketballPlayer::POSITION_CENTER, 'Foo', [1, 2, 3]),
            new BasketballPlayer(BasketballPlayer::POSITION_CENTER, 'Bar', [1, 2, 3]),
            new BasketballPlayer(BasketballPlayer::POSITION_CENTER, 'Baz', [1, 2, 3]),
        ]);

        $this->assertEquals(39, (new BasketballMatchCalculator())->calculate($match));
    }

}
