<?php

namespace Tests\Calculator;

use Manana\Calculator\BasketballMatchCalculator;
use Manana\Calculator\CalculatorFactory;
use Manana\Model\Match;
use PHPUnit\Framework\TestCase;

/**
 * CalculatorFactoryTest class.
 *
 * @package Tests\Calculator
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class CalculatorFactoryTest extends TestCase
{

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Unsupported match type "Foo"
     */
    public function testFactory()
    {
        $this->assertInstanceOf(BasketballMatchCalculator::class, CalculatorFactory::factory(Match::TYPE_BASKETBALL));
        $this->assertInstanceOf(BasketballMatchCalculator::class, CalculatorFactory::factory(Match::TYPE_BASKETBALL));
        $this->assertInstanceOf(BasketballMatchCalculator::class, CalculatorFactory::factory('Foo'));
    }

}
