<?php

namespace Tests\Calculator;

use Manana\Calculator\HandballMatchCalculator;
use Manana\Model\HandballPlayer;
use Manana\Model\Match;
use PHPUnit\Framework\TestCase;

/**
 * HandballMatchCalculatorTest class.
 *
 * @package Tests\Calculator
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class HandballMatchCalculatorTest extends TestCase
{

    public function testCalculate()
    {
        $match = new Match(Match::TYPE_BASKETBALL, [
            new HandballPlayer(HandballPlayer::POSITION_GOALKEEPER, 'Foo', [1, 2]),
            new HandballPlayer(HandballPlayer::POSITION_FIELD_PLAYER, 'Bar', [1, 2]),
        ]);

        $this->assertEquals(0, (new HandballMatchCalculator())->calculate($match));
    }

}
