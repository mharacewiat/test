<?php

namespace Tests\Importer;

use Manana\Importer\MatchesImporter;
use Manana\Model\Match;
use Manana\Reader\MatchReaderInterface;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;

/**
 * MatchesImporterTest class.
 *
 * @package Tests\Importer
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class MatchesImporterTest extends TestCase
{

    /**
     * @var vfsStreamDirectory
     */
    protected $root;


    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
        $this->root = vfsStream::setup('root', null, [
            'foo.txt' => 'foo',
            'bar.txt' => 'bar',
            'baz.txt' => 'baz',
        ]);
    }

    /**
     * Test whether import passed logic to reader properly, and if properly mapped it's output.
     */
    public function testImport()
    {
        $importer = $this
            ->getMockBuilder(MatchesImporter::class)
            ->setMethods(['processPlayers'])
            ->getMock();

        $importer
            ->expects($this->any())
            ->method('processPlayers')
            ->willReturnOnConsecutiveCalls(
               $fooPlayers = [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
               $barPlayers = [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
               $bazPlayers = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
            );

        $reader = $this
            ->getMockBuilder(MatchReaderInterface::class)
            ->setMethods(['read'])
            ->getMock();

        $reader
            ->expects($this->any())
            ->method('read')
            ->withConsecutive(
                [vfsStream::url('root/foo.txt')],
                [vfsStream::url('root/bar.txt')],
                [vfsStream::url('root/baz.txt')]
            )
            ->willReturnOnConsecutiveCalls(
                ['FOO', $fooPlayers],
                ['BAR', $barPlayers],
                ['BAZ', $bazPlayers]
            );

        /** @var MatchesImporter $importer */
        $matches = $importer->import(vfsStream::url('root'));

        /** @var Match $fooMatch */
        $fooMatch = $matches[0];

        /** @var MatchReaderInterface $reader */
        $this->assertInstanceOf(Match::class, $fooMatch);
        $this->assertEquals('foo', $fooMatch->getType());
        $this->assertCount(3, $fooMatch->getPlayers());
    }

}
