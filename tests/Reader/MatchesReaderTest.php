<?php

namespace Tests\Reader;

use Manana\Reader\MatchReader;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;

/**
 * MatchesReaderTest class.
 *
 * @package Tests\Reader
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class MatchesReaderTest extends TestCase
{

    /**
     * @var vfsStreamDirectory
     */
    protected $root;


    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
        $this->root = vfsStream::setup('root', null, []);
    }


    /**
     * Test whether source file is parsed properly or not.
     */
    public function testImportMatches()
    {
        $testContents = <<<EOL
FOO
Bar;Baz;Qux
Qux;Baz;Bar
EOL;

        vfsStream::newFile('test.csv')
            ->at($this->root)
            ->setContent($testContents);

        $this->assertSame(
            [
                'FOO',
                [
                    [
                        'Bar',
                        'Baz',
                        'Qux',
                    ],
                    [
                        'Qux',
                        'Baz',
                        'Bar',
                    ]
                ]
            ],
            (new MatchReader())->read(vfsStream::url('root/test.csv'))
        );
    }

}
