<?php

namespace Manana\Model;

/**
 * Player class.
 *
 * @package Manana\Model
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
abstract class Player
{

    /**
     * @var string
     */
    protected $position;

    /**
     * @var string
     */
    protected $team;

    /**
     * @var array
     */
    protected $points;


    /**
     * @var int
     */
    protected $score = 0;


    /**
     * Player constructor.
     *
     * @param string $position
     * @param string $team
     * @param array $points
     */
    public function __construct(string $position, string $team, array $points)
    {
        $this->position = $position;
        $this->team = $team;
        $this->points = $points;
    }


    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @return array
     */
    public function getPoints(): array
    {
        return $this->points;
    }


    /**
     * @param int $score
     * @return Player
     */
    public function setScore(int $score): Player
    {
        $this->score = $score;
        return $this;
    }

}
