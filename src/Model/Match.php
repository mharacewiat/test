<?php

namespace Manana\Model;

class Match
{

    /**
     * @var string
     */
    const TYPE_BASKETBALL = 'BASKETBALL';

    /**
     * @var string
     */
    const TYPE_HANDBALL = 'HANDBALL';


    /**
     * @var string
     */
    protected $type;

    /**
     * @var iterable|Player[]
     */
    protected $players;


    /**
     * @var int
     */
    protected $score = 0;


    /**
     * Match constructor.
     *
     * @param string $type
     * @param iterable|Player[] $players
     */
    public function __construct(string $type, iterable $players)
    {
        $this->type = $type;
        $this->players = $players;
    }


    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return iterable|Player[]
     */
    public function getPlayers(): iterable
    {
        return $this->players;
    }


    /**
     * @param int $score
     * @return Match
     */
    public function setScore(int $score): Match
    {
        $this->score = $score;
        return $this;
    }

}
