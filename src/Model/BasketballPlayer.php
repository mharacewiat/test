<?php

namespace Manana\Model;

/**
 * BasketballPlayer class.
 *
 * @package Manana\Model
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class BasketballPlayer extends Player
{

    /**
     * @var string
     */
    const POSITION_GUARD = 'G';

    /**
     * @var string
     */
    const POSITION_FORWARD = 'F';

    /**
     * @var string
     */
    const POSITION_CENTER = 'C';

}
