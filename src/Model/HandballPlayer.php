<?php

namespace Manana\Model;

/**
 * HandballPlayer class.
 *
 * @package Manana\Model
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class HandballPlayer extends Player
{

    /**
     * @var string
     */
    const POSITION_GOALKEEPER = 'G';

    /**
     * @var string
     */
    const POSITION_FIELD_PLAYER = 'F';

}
