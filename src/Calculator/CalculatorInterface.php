<?php

namespace Manana\Calculator;

use Manana\Model\Match;

/**
 * CalculatorInterface interface.
 *
 * @package Manana\Calculator
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
interface CalculatorInterface
{

    /**
     * @param Match $match
     * @return int
     */
    public function calculate(Match $match): int;

}
