<?php

namespace Manana\Calculator;

use Manana\Model\Match;
use Manana\Model\Player;

/**
 * MatchCalculator class.
 *
 * @package Manana\Calculator
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
abstract class MatchCalculator implements CalculatorInterface
{

    /**
     * @var array
     */
    protected static $weights = [];


    /**
     * @param Match $match
     * @return int
     */
    public function calculate(Match $match): int
    {
        $matchScore = 0;

        foreach ($match->getPlayers() as $player) {
            try {
                $playerScore = $this->calculatePlayerScore($player);
            } catch (\Exception$exception) {
                continue;
            }

            $player->setScore($playerScore);
            $matchScore += $playerScore;
        }

        $match->setScore($matchScore);
        return $matchScore;
    }


    /**
     * @param Player $player
     * @return int
     * @throws \Exception
     */
    protected function calculatePlayerScore(Player $player): int
    {
        $weights = static::$weights[$player->getPosition()];
        $points = $player->getPoints();

        if (count($points) != count($weights)) {
            throw new \Exception('Unequal score size');
        }

        $playerScore = 0;

        foreach ($points as $key => $point) {
            $playerScore += ($point * $weights[$key]);
        }

        return $playerScore;
    }

}
