<?php

namespace Manana\Calculator;

use Manana\Model\Match;

/**
 * CalculatorFactory class.
 *
 * @package Manana\Calculator
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class CalculatorFactory
{

    /**
     * @param string $type
     * @return CalculatorInterface
     * @throws \Exception
     */
    public static function factory(string $type): CalculatorInterface
    {
        switch ($type) {
            case Match::TYPE_BASKETBALL:
                return new BasketballMatchCalculator();
            case Match::TYPE_HANDBALL:
                return new HandballMatchCalculator();
            default:
                /* Do nothing. */
                break;
        }

        throw new \Exception(sprintf('Unsupported match type "%s"', $type));
    }

}
