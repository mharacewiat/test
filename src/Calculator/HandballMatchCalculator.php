<?php

namespace Manana\Calculator;

use Manana\Model\HandballPlayer;

/**
 * HandballMatchCalculator class.
 *
 * @package Manana\Calculator
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class HandballMatchCalculator extends MatchCalculator
{

    /**
     * {@inheritdoc}
     */
    protected static $weights = [
        HandballPlayer::POSITION_GOALKEEPER => [
            5,
            -2,
        ],
        HandballPlayer::POSITION_FIELD_PLAYER => [
            1,
            -1,
        ],
    ];

}
