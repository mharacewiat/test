<?php

namespace Manana\Calculator;

use Manana\Model\BasketballPlayer;

/**
 * BasketballMatchCalculator class.
 *
 * @package Manana\Calculator
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class BasketballMatchCalculator extends MatchCalculator
{

    /**
     * {@inheritdoc}
     */
    protected static $weights = [
        BasketballPlayer::POSITION_GUARD => [
            2,
            3,
            1,
        ],
        BasketballPlayer::POSITION_FORWARD => [
            2,
            2,
            2,
        ],
        BasketballPlayer::POSITION_CENTER => [
            2,
            1,
            3,
        ],
    ];

}
