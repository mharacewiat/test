<?php

namespace Manana\Importer;

use Manana\Model\Match;
use Manana\Model\Player;
use Manana\Processor\MatchPlayerProcessorFactory;
use Manana\Reader\MatchReader;
use Manana\Reader\MatchReaderInterface;

/**
 * MatchesImporter class.
 *
 * @package Manana\Importer
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class MatchesImporter
{

    /**
     * @var MatchReader
     */
    protected $reader;


    /**
     * MatchesImporter constructor.
     *
     * @param MatchReaderInterface|null $reader
     */
    public function __construct(MatchReaderInterface $reader = null)
    {
        $this->reader = ($reader ?? new MatchReader());
    }


    /**
     * @param string $directory
     * @return iterable|Match[]
     */
    public function import(string $directory): iterable
    {
        $matches = [];
        $source = $this->read($directory);

        foreach ($source as $match) {
            /**
             * @var string $type
             * @var iterable|Player[] $players
             */
            list ($type, $players) = $match;

            try {
                $matches[] = new Match($type, $this->processPlayers($type, $players));
            } catch (\Exception $exception) {
                continue;
            }
        }

        return $matches;
    }

    /**
     * @param string $directory
     * @return iterable
     */
    protected function read(string $directory): iterable
    {
        $source = [];
        $directory = new \DirectoryIterator($directory);

        foreach ($directory as $file) {
            if (false == $file->isFile()) {
                continue;
            }

            $source[$file->getBasename('.txt')] = $this->reader->read($file->getPathname());
        }

        return $source;
    }

    /**
     * @param string $type
     * @param iterable $data
     * @return iterable
     * @throws \Exception
     */
    protected function processPlayers(string $type, iterable $data): iterable
    {
        $players = [];

        foreach ($data as $p) {
            $players[] = MatchPlayerProcessorFactory::factory($type, $p);
        }

        return $players;
    }

}
