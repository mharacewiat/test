<?php

namespace Manana\Reader;

/**
 * MatchReader class.
 *
 * @package Manana\Reader
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class MatchReader implements MatchReaderInterface
{

    /**
     * {@inheritdoc}
     */
    public function read(string $path): array
    {
        $source = $this->readSource($path);
        return [array_shift($source), $source];
    }


    /**
     * @param string $path
     * @return array
     */
    protected function readSource(string $path): array
    {
        $source = [];

        $handle = fopen($path, 'r');

        while ($line = fgets($handle)) {
            try {
                $source[] = $this->processSourceLine(trim($line));
            } catch (\Exception $exception) {
                continue;
            }
        }

        fclose($handle);

        return $source;
    }

    /**
     * @param string $line
     * @return string|array
     * @throws \Exception
     */
    protected function processSourceLine(string $line)
    {
        if (0 < preg_match_all('/([^;]+)/i', $line, $matches)) {
            list(, $chunks) = $matches;

            if (1 < count($chunks)) {
                return $chunks;
            }

            return array_pop($chunks);
        }

        throw new \Exception('Unprocessable source line');
    }

}
