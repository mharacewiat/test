<?php

namespace Manana\Reader;

/**
 * MatchReaderInterface interface.
 *
 * @package Manana\Reader
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
interface MatchReaderInterface
{

    /**
     * Return match details from given file.
     *
     * First element should be considered as match type; rest of the result are players details.
     *
     * @example
     *  [
     *      'FOO',
     *      [
     *          'Bar',
     *          'Baz',
     *          'Qux',
     *      ]
     *  ]
     *
     * @param string $path
     * @return array
     */
    public function read(string $path): array;

}
