<?php

namespace Manana\Processor;

use Manana\Model\BasketballPlayer;
use Manana\Model\HandballPlayer;
use Manana\Model\Match;
use Manana\Model\Player;

/**
 * MatchPlayerProcessorFactory class.
 *
 * @package Manana\Processor
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class MatchPlayerProcessorFactory
{

    /**
     * @param string $type
     * @param array $data
     * @return Player
     * @throws \Exception
     */
    public static function factory(string $type, array $data)
    {
        switch ($type) {
            case Match::TYPE_BASKETBALL:
                return new BasketballPlayer($data[4], $data[3], [$data[5], $data[6], $data[7]]);
            case Match::TYPE_HANDBALL:
                return new HandballPlayer($data[4], $data[3], [$data[5], $data[6]]);
            default:
                /** Do nothing. */
                break;
        }

        throw new \Exception(sprintf('Unsupported match type "%s"', $type));
    }

}
