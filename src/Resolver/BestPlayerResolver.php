<?php

namespace Manana\Resolver;

use Manana\Model\Match;

/**
 * BestPlayerResolver class.
 *
 * @package Manana\Resolver
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class BestPlayerResolver
{

    /**
     * @param iterable|Match[] $matches
     * @return Match
     */
    public function resolve(iterable $matches): Match
    {
        return $matches[0]->getPlayers()[0];
    }

}
