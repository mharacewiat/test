<?php

namespace Manana\Resolver;

use Manana\Model\Match;

/**
 * WinningTeamResolver class.
 *
 * @package Manana\Resolver
 * @author Michal Haracewiat <haruni000@gmail.com>
 */
class WinningTeamResolver
{

    /**
     * @param iterable|Match[] $matches
     * @return Match
     */
    public function resolve(iterable $matches): Match
    {
        return $matches[0];
    }

}
